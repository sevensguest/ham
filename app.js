// our services

$('.services-normal').click(function () {
    $('.services-active').removeClass('services-active');
    $(this).addClass('services-active');
    let page = $(this).index();

    $('.services-content').addClass('hidden');
    $('.down').addClass('hidden');
    $('.services-content').eq(page).removeClass('hidden');
    $('.down').eq(page).removeClass('hidden');

});


// amazing work

$('.work>li').click(function () {
    let page = $(this).index();
    $('.work-gallery>img').addClass('hidden');
    $('.work>li').css({"color": "#717171", "border-top": "1px #EBEBEB solid"});
    $(this).css({"color": "#18CFAA", "border-top": "2px #18CFAB solid"});


    switch (page) {
        case 0:
            $('.work-gallery>img').removeClass('hidden');
            break;
        case 1:
            $('.gd').removeClass('hidden');
            break;
        case 2:
            $('.wd').removeClass('hidden');
            break;
        case 3:
            $('.lp').removeClass('hidden');
            break;
        case 4:
            $('.wp').removeClass('hidden');
            break;
    }
});

$('.work-gallery>img').slice(0, 24).removeClass('hidden');

//  load more

$(".load-more").on('click', function (e) {
    e.preventDefault();

    $(".load-more").remove();
    $("#loader").removeClass('hidden');

    setTimeout(()=> {
        $("#loader").addClass('hidden');
        $('.work-gallery>img').slideDown();
    }, 1000);

});

// What People Say About theHam
//  initialization

$('.persons>div').addClass('hidden');
$('.persons>div').eq(0).removeClass('hidden');
$('.faces').eq(0).addClass('faces-active');


// What People Say About theHam

page = 0;

$('.faces').click(function () {
    page = $(this).index() - 1;
    moveSlides();
});

$('.ar-right').click(function () {
    if (page >=0 && page <3){
    page += 1;
    moveSlides();
    }
});

$('.ar-left').click(function () {
    if (page>0 && page <4){
        page -= 1;
        moveSlides();
    }
});

function moveSlides(){
    $('.persons>div').addClass('hidden');
    $('.persons>div').eq(page).removeClass('hidden');
    $('.faces').removeClass('faces-active');
    $('.faces').eq(page).addClass('faces-active');
}